package gt.edu.umg.crm.backend.repository;

import gt.edu.umg.crm.backend.entity.Company;
import gt.edu.umg.crm.backend.entity.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CompanyRepository extends JpaRepository<Company,Long> {
    @Query("select distinct c.name from Company c " +
            "where lower(c.name) like lower(concat('%', :searchTerm, '%'))")
    List<Company> search(@Param("searchTerm") String searchTerm);

}
