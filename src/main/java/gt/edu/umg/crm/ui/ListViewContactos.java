package gt.edu.umg.crm.ui;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import gt.edu.umg.crm.backend.entity.Company;
import gt.edu.umg.crm.backend.entity.Contact;
import gt.edu.umg.crm.backend.service.CompanyService;
import gt.edu.umg.crm.backend.service.ContactService;


@Route(value = "direcciones", layout = MainLayout.class)
@PageTitle("Direcciones | Vaadin Ejemplo")
public class ListViewContactos extends VerticalLayout {


    private CompanyService servicio;
    Grid<Company> grid = new Grid<>(Company.class);
    TextField filterText = new TextField();

    public ListViewContactos(CompanyService servicio) {
        this.servicio = servicio;
        addClassName("list-view");
        setSizeFull();
        configureGrid();


        Div content = new Div(grid);
        content.addClassName("content");
        content.setSizeFull();

        add(getToolBar(),content);
        updateList();

    }

    private void configureGrid(){
        grid.addClassName("contact-grid");
        grid.setSizeFull();
        grid.removeColumnByKey("name");
       grid.setColumns("name");
    }
        private void updateList(){
            grid.setItems(servicio.findAll(filterText.getValue()));

    }

    private Component getToolBar(){
        filterText.setPlaceholder("Nombre");
        filterText.setClearButtonVisible(true);
        filterText.setValueChangeMode(ValueChangeMode.LAZY);
        filterText.addValueChangeListener(e -> updateList());

        HorizontalLayout toolBar = new HorizontalLayout(filterText);
        return toolBar;
    }

}
